CodeKraft React Library
=======================

This lib is published to [NPM](https://www.npmjs.com/package/codekraft-react-frontend).

You can use following docker-compose service configuration to build and watch
on development process:

```yaml
  react:
    image: node
    working_dir: /codekraft
    entrypoint: /codekraft/entrypoint
    volumes:
      - ./codekraft-react-frontend:/codekraft
```

Lerna
=====

Version 2.\* of the lib is developed as multi-packages monorepo using
[Lerna](https://github.com/lerna/lerna).

To publish a new version if changes are made on lib/\* folders, it is very
simple using following command and following instructions:

```
lerna publish
```
