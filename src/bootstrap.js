import React from "react"

import ReactDOM from "react-dom"
import { connect, Provider } from "react-redux"
import { Router } from "react-router-dom"
import Popup from "react-popup"
import ReactGA from "react-ga"

import Logger from "js-logger"
Logger.useDefaults()

import createStore from "./api/client/reducer/create-store"
import createClients from "./api/client/create-clients"
import createNavigation from "./navigation/create-navigation"

import App from "./components/app"
import AppV2 from "./components/v2/app"
import ScrollToTop from "./components/common/scroll-to-top"

import BootstrapConfig from "./config/navigation/default"
import BootstrapConfigV2 from "./config/navigation/v2/default"

import createBrowserHistory from "history/createBrowserHistory"
import createHashHistory from "history/createHashHistory"

import moment from "moment-timezone"
moment.locale("fr")

if (process.env.UA_ID && typeof document !== "undefined") {
	ReactGA.initialize(process.env.UA_ID)
}

const collect = (location) => {
	if (process.env.UA_ID && typeof document !== "undefined") {
		ReactGA.set({ page: location.pathname })
		ReactGA.pageview(location.pathname)
	}
}

const setTitle = (location, config) => {
	let splittedNav = location.pathname.split('/')
	let root = splittedNav[1] || "offline"
	let page = splittedNav.length > 2 ? splittedNav[2] : null
	let rootConfig = config.navigation[root]
	if (!rootConfig) {
		page = root
		root = "offline"
		rootConfig = config.navigation[root]
	}
	let items = rootConfig.menu.default.items
	let pageConfig = page ? items.find(i => i.route == page) : items.find(i => i.root)
	if (pageConfig && config.mainTitle) {
		document.title = `${pageConfig.title + " - " || ""}${config.mainTitle}`
	}
}

function launch(config, callback) {
	const store = createStore(config)
	const clients = createClients(config.clients, store)
	const version = config.version ? config.version : 1

	setTitle(location, config)
	collect(location)
	const history = config.history == "hash" ? createHashHistory() : createBrowserHistory()
	history.listen((location, action) => {
		setTitle(location, config)
		collect(location)
	})
	if (config.listenHistory) {
		history.listen(config.listenHistory(store, history))
	}

	let mainComponent = App, bootstrapConfig = BootstrapConfig
	if (version === 2) {
		mainComponent = AppV2
		bootstrapConfig = BootstrapConfigV2
	}

	store.dispatch({
		type: "CLIENTS",
		clients: clients
	})

	createNavigation(bootstrapConfig, config.navigation, clients, function(nav) {
		store.dispatch({
			type: "NAVIGATION",
			navigation: nav
		})

		const App = () => (
			<Provider store={store}>
				<Router history={history}>
					<ScrollToTop>
						{React.createElement(mainComponent, {config: config})}
					</ScrollToTop>
				</Router>
			</Provider>
		)

		ReactDOM.render(
			<App />,
			document.getElementById("app-root")
		)

		ReactDOM.render(
			<Provider store={store}>
				<Router history={history}>
					<Popup
						escToClose={true}
						closeOnOutsideClick={false}
						defaultOk="OK"
						defaultCancel="Annuler"
						className={(config.popup && config.popup.className) ? config.popup.className : "mm-popup"}
						btnClass={(config.popup && config.popup.bntClass) ? config.popup.btnClass : "mm-popup__btn"}
						wildClasses={!config.popup}
					/>
				</Router>
			</Provider>,
			document.getElementById("popup-container")
		)

		if (callback) {
			callback(store, clients)
		}
	})
}

import render from './bootstrap/render'
import server from './bootstrap/server'

export default {
	launch,
	render,
	server
}
