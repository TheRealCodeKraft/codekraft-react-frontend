import React from "react"
import { connect } from 'react-redux'

import Form from '../utils/form'
import { Link } from 'react-router-dom'

class ForgotPassword extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      found: undefined
    }

    this.handleSubmitComplete = this.handleSubmitComplete.bind(this)
  }

  render() {
    var content = null
    switch(this.state.found) {
      case undefined:
				content = this.renderForm()
        break
      case true:
        content = this.found()
        break
      case false:
        content = [
          <span className="forgot-email-no-account-found">Aucun compte n'a été identifié avec cette adresse email</span>,
          this.renderForm()
        ]
        break
    }

    return <div>{content}</div>
  }

  renderForm() {
		const strings = this.strings()

    return [
      <Form id="login-form" key="login-form"
            clients={this.props.clients}
						ref={this.props.formRef ? this.props.formRef : (ref) => { this.form = ref }}
            fields={[
              {
                name: "email",
                label: strings.emailLabel,

                title: "Email",
                placeholder: "Email",
                type: "text",
                required: true
              }
            ]}
						fieldWrapper={this.props.fieldWrapper}
						submitClass={this.props.submitClass || "btn btn-accent btn-signup"}
            submitLabel={this.props.submitLabel || "Envoyer"}
						hideSubmit={this.props.hideSubmit || false}
						submitWrapper={this.props.submitWrapper}
            onSubmit={this.handleSubmit}
						onSubmitError={this.handleSubmitError}
						errors={this.props.errors}
            service={{client: this.props.clients.UserClient, func: "forgotPassword"}}
            onSubmitComplete={this.handleSubmitComplete}
						cancelButton={this.props.cancelButton}
						cancelLabel={this.props.cancelLabel}
						cancelClass={this.props.cancelClass}
						onCancel={this.props.onCancel}
						mapErrorMessage={this.props.mapErrorMessage}
       >
				{this.props.children}
			</Form>,
			 this.props.showLoseLinks
	      ? [<Link className={"btn btn-default"} to="/login">J'ai déjà un compte</Link>,
      		<Link className={"btn btn-default"} to="/signup">Créer un compte</Link>]
				: null
			]
  }

  found() {
		const strings = this.strings()

    return [
      <span key="sent-string" className="message-forgot-email-sent">{strings.sent}</span>,
      <Link to='/' key="back-to-home" className="forgot-password-link-back-to-home">Retour à la page d'accueil</Link>]
  }

	strings() {
		return Object.assign({}, {
			emailLabel: "Email",
			sent: "Yay! Un email vous a été envoyé pour vous permettre de réinitialiser votre mot de passe"
		}, this.props.strings)
	}

  handleSubmitComplete(data) {
		if (this.props.onSubmitComplete) {
			this.props.onSubmitComplete(data)
		}

		if (this.props.onSubmitFound) {
			this.props.onSubmitFound(data)
		}

	  this.setState({found: data.found})
  }

  handleSubmitError = (data) => {
		if (this.props.onSubmitError) {
			this.props.onSubmitError(data)
		}
  }

}

function mapStateToProps(state) {
  return {
    clients: state.bootstrap.clients,
    newUser: state.userState.newUser || null,
    passwordUpdated: state.userState.password_updated || null
  }
}

export default connect(mapStateToProps)(ForgotPassword)
