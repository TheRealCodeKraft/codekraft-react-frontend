
import moment from 'moment-timezone'
moment.locale('fr')

import isomorphic from './isomorphic'
import createNavigation from '../navigation/create-navigation'

import App from '../components/app'
import BootstrapConfig from '../config/navigation/default'

export default function server({ config, process, env }, callback) {
	// Dont know why I have to do this total mess
	process.env = env

	const { store, clients, version } = isomorphic(config)

	createNavigation(BootstrapConfig, config.navigation, clients, function(navigation) {
		store.dispatch({
			type: "NAVIGATION",
			navigation
		})

		callback({ App, store, config })
	})
}
