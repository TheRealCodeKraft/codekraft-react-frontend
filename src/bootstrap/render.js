
const React = require('react')
const renderToString = require('react-dom/server').renderToString

import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router-dom'

export default function render({ App, Html, store, config, request, context = {} }, callback) {
	const body =

	callback(Html({
		body: renderToString(
			<Provider store={store}>
				<StaticRouter location={request.url} context={context}>
					{React.createElement(App, { config })}
				</StaticRouter>
			</Provider>
		),
		config,
		request
	}))
}
