import createStore from '../api/client/reducer/create-store'
import createClients from '../api/client/create-clients'

export default function isomorphic(config) {
	const store = createStore(config)
	const clients = createClients(config.clients, store)
	const version = config.version ? config.version : 1

	store.dispatch({
		type: "CLIENTS",
		clients
	})

	return {
		store,
		clients,
		version
	}
}
