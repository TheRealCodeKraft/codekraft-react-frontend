import OfflineHome from '../components/offline/home'
import DashboardHome from '../components/dashboard/home'
import AdminHome from '../components/admin/home'

import Page from '../components/common/page'

function loadItems(mainKey, subKey, group, client, callback) {
	client.fetchAll({ group }, (items) => {
		for (let key in items) {
			if (items[key].slug) {
				items[key].route = items[key].slug
				if (items[key].component) item[key].component(items[key])
				else items[key].component = Page(items[key])
			}
		}
		callback(mainKey, subKey, items)
	}, true)
}

function loadItem(mainKey, subKey, itemIndex, baseItem, data, clients, callback) {
	const client = clients[data.client + "Client"]
	client.fetchAll(data.params, (data) => {
		let item = data[0]
		for(let key in item) {
			baseItem[key] = item[key]
		}
		item = baseItem
		item.component = Page(item)

		callback(mainKey, subKey, itemIndex, item)
	}, true)
}

module.exports = function(BootstrapConfig, config, clients, callback) {
  var lastIndex
  for (var key in BootstrapConfig) {
    if (!config[key]) config[key] = BootstrapConfig[key]
    else {
      if (!config[key].root) config[key].root = BootstrapConfig[key].root
      if (!config[key].restricted && BootstrapConfig[key].restricted) config[key].restricted = BootstrapConfig[key].restricted
      if (!config[key].grants && BootstrapConfig[key].grants) config[key].grants = BootstrapConfig[key].grants
      if (config[key].enableDefault !== false) {
        if (!config[key].menu) config[key].menu = BootstrapConfig[key].menu
        else {
          for (var menuKey in BootstrapConfig[key].menu) {
            if (!config[key].menu[menuKey]) config[key].menu[menuKey] = BootstrapConfig[key].menu[menuKey]
            else {
              if (!config[key].menu[menuKey].items) config[key].menu[menuKey].items = []
              for (var itemKey in BootstrapConfig[key].menu[menuKey].items) {
                if (BootstrapConfig[key].menu[menuKey].items[itemKey].root) {
                  if (config[key].menu[menuKey].items.filter(item => { return item.root }).length > 0) {
                    continue
                  }
                }
                if (BootstrapConfig[key].menu[menuKey].items[itemKey].route !== undefined && config[key].menu[menuKey].items.filter(item => { return item.route == BootstrapConfig[key].menu[menuKey].items[itemKey].route }).length > 0) {
                  continue
                }
                config[key].menu[menuKey].items.push(BootstrapConfig[key].menu[menuKey].items[itemKey])
                lastIndex = config[key].menu[menuKey].items.length - 1
                if (config[key].menu[menuKey].discards && config[key].menu[menuKey].discards.indexOf(config[key].menu[menuKey].items[lastIndex].title) !== -1) {
                  config[key].menu[menuKey].items[lastIndex].display = false
                }
              }
            }
          }
        }
      }
    }
  }

	const scope = {
		listCounter: 0,
		itemCounter: 0,
		callback,
		clients,
		config,
		async: false
	}

	Object.entries(config)
		.filter(([ key, config ]) => config.menu !== undefined)
		.map(mapConfig.bind(scope))

	if (!scope.async) {
		callback(config)
	}
}

function mapConfig([ key, config ]) {
	Object.entries(config.menu)
		.map(mapMenu.bind(this, [ key, config ]))
}

function mapMenu(menu, [ submenuKey, submenu ]) {
	if (submenu.source) {
		mapSource.call(this, menu, [ submenuKey, submenu ])
	} else if (submenu.items) {
		mapItems.call(this, menu, [ submenuKey, submenu ])
	}
}

function mapSource([ key, menu ], [ submenuKey, submenu ]) {
	const self = this
	self.async = true

	const { clients, callback, config } = self

	const client = clients[submenu.source.client + "Client"]
	self.listCounter++
	loadItems(key, submenuKey, submenu.source.group, client, function(mainKey, subKey, items) {
		config[mainKey].menu[subKey].items = items
		self.listCounter--
		if (self.listCounter === 0 && self.itemCounter === 0) {
			callback(config)
		}
	})
}

function mapItems([ key, menu ], [ submenuKey, submenu ]) {
	const self = this
	const { config, callback, clients } = self

	Object.entries(submenu.items)
		.map(([ itemKey, item]) => {
			if (item.dynamic) {
				self.async = true
				self.itemCounter++
				loadItem(key, submenuKey, itemKey, submenu.items[submenuKey], item.dynamic, clients, function(mainKey, subKey, itemKey, item) {
					config[mainKey].menu[subKey].items[itemKey] = item
					self.itemCounter--
					if (self.listCounter === 0 && self.itemCounter === 0) {
						callback(config)
					}
				})
			}
		})
}
